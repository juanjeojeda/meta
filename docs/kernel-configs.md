# Kernel configs for upstream kernels

Upstream kernels are built using the latest available Fedora kernel
configurations from the latest stable release. This allows us to build and
test kernels with a common set of kernel configurations that are widely used.

## Generating configs

The kernel configuration files are available in the [kernel-configs]
repository in GitLab. They are kept up to date with a daily pipeline run at
4AM UTC that is [scheduled in GitLab].

The update process involves:

* Using `dnf` to download the `kernel-core` package for each architecture
* The kernel config is extracted from the downloaded RPM
* If the kernel configs are new, they are committed to the repository

The pipelines can then pull the kernel configs directly from GitLab in raw
format and use them to build kernels.

[kernel-configs]: https://gitlab.com/cki-project/kernel-configs
[scheduled in GitLab]: https://gitlab.com/cki-project/kernel-configs/pipeline_schedules
