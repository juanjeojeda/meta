# Diagram cheat sheet

All diagrams are created via [diagrams.net] (formerly draw.io) and stored as
PNGs with embedded XML. Their source code can be found on
[GitHub](https://github.com/jgraph/drawio).

As file type, always use `Editable bitmap image (PNG)`. That way, reviews on
GitLab show the image differences, which is not the case for `SVG`.

## Creating and modifying diagrams locally

Install `drawio-desktop` either as a
[rpm/deb/snap package](https://github.com/jgraph/drawio-desktop/releases/) or
as a Flatpak via
[Flathub](https://flathub.org/apps/details/com.jgraph.drawio.desktop).

## Creating and modifying diagrams online

Fork the [meta] repository into your own account. Clone the fork, create a new
branch for your changes, push it to GitLab and create a new merge request via

```shell
git switch -c new-branch-name
git push origin -o merge_request.create
```

Open the merge request in a web browser, and enable `Squash commits` via the
`Edit` button on top.

Go to [diagrams.net] and select `GitLab` storage. When asked, authorize the app
in GitLab.

Click either on `Create New Diagram` or `Open Existing Diagram`. Navigate to
your **fork** of the meta repo, and click on the branch name in the top row.
Select the branch that was created above, and navigate to `docs/diagrams`.

As file type, always use `Editable bitmap image (PNG)`. That way, reviews on
GitLab show the image differences, which is not the case for `SVG`.

When a diagram is saved, a new commit is added to the newly created branch, and
the merge request is updated automatically.

## Changing diagram resolution

To change the PNG image dimensions, increase the zoom level at
`File`/`Properties`/`Zoom`.

## Extracting the XML code from PNGs

The compressed XML data is stored in a [`zTxt` section in the
PNGs](https://www.diagrams.net/blog/xml-in-png). It can be extracted via
something like `pnginfo` from [pngtools]. The uncompressed XML data can be
obtained via URL-decoding, base64-decoding and inflating, e.g. with
[drawio-tools].

[diagrams.net]: https://app.diagrams.net/
[drawio-desktop]: https://github.com/jgraph/drawio-desktop/releases/
[meta]: https://gitlab.com/cki-project/meta/-/forks/new
[pngtools]: https://packages.debian.org/source/pngtools
[drawio-tools]: https://jgraph.github.io/drawio-tools/tools/convert.html
