# CKI Project Documentation

This repository contains public documentation for the CKI Project.

## General documentation

* [Creating new projects](new-projects) - Want to create a new repo? Start here.
* [Descriptions of known CKI issues](failures) - Pipeline is tagged with an
  issue in the Data Warehouse? Check out the details!
* [LTP update procedure](ltp-update)

## CKI Onboarding

* [Enable Testing for your Kernel Tree](onboarding)
* [Coding guidelines](coding)
* [Contributing to LTP](onboarding#ltp)
* [Contributing to kselftests](onboarding#kselftests)
* [Contributing a Standalone Test](onboarding#standalone-tests)

## Operations

* [Kernel configs for upstream kernels](kernel-configs)
* [Updating CKI containers](updating-containers)
